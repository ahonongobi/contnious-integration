# DevOps Learning Journey -  CI/CD

This repository contains a summary of my learning journey in the field of DevOps. I've gained valuable knowledge and skills in various areas related to development, deployment, and operations.

## What I've Learned

### Git and GitLab
- Created and managed Git repositories for version control.
- Utilized GitLab for Continuous Integration and Continuous Deployment (CI/CD).
- Automated deployment pipelines using GitLab CI/CD.

### Ansible
- Configured and automated system management tasks using Ansible.
- Developed Ansible playbooks to define desired server states.
- Managed server configuration and application deployments.

### Docker
- Created and managed containers to isolate applications and dependencies.
- Simplified application deployment using Docker containers.

### AWS EC2
- Launched and managed EC2 instances for hosting applications.
- Used SSH keys for secure access to instances.
- Configured security group rules for controlling instance access.

### Continuous Deployment
- Established CI/CD pipelines for automating deployments.
- Employed scripts and playbooks to deploy applications to remote servers.

### Jenkins
- Installed and configured Jenkins for automated pipeline execution.
- Integrated Jenkins plugins for Git, AWS, SSH, and more.
- Created jobs to orchestrate build, test, and deployment stages.
- Configured Git integration to pull source code for automation.
- Set up GitHub webhooks to enable automated builds triggered by code changes.
- Utilized the "Publish Over SSH" plugin for secure deployment to remote servers.
- Implemented scheduled pipeline execution by configuring Jenkins to run the pipeline every hour during deployment.
- Set up email notifications to receive alerts on build and deployment status.

### RESTful API
- Understood RESTful API concepts for inter-application communication.
- Utilized RESTful APIs for data exchange between different services.

### Agile Methodology (SCRUM)
- Embraced agile methodologies like SCRUM for effective project management.
- Collaborated within agile teams to develop and deploy projects.

### Web Development
- Utilized web technologies like PHP, Apache, Nginx for application deployment.
- Created web projects with user interfaces and application logic.

### Cloud Computing
- Gained an introduction to cloud computing concepts, including AWS.
- Hosted and deployed applications in cloud environments.

### Security
- Managed SSH keys for securing server access.
- Implemented security group rules to restrict EC2 instance access.

### Automation
- Automated repetitive tasks using scripts and tools like Ansible.

## Conclusion

My journey in DevOps has equipped me with a diverse skill set to effectively collaborate in modern application development, deployment, and management. This repository serves as a reminder of my progress and a reference for the concepts and tools I've learned.
